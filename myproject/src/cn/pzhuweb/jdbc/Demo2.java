package cn.pzhuweb.jdbc;

import com.mysql.cj.protocol.Resultset;

import java.sql.*;

public class Demo2 {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://127.0.0.1:3306/test";
        Connection con = DriverManager.getConnection(url, "root", "root");

       String sql = "insert into fortest value(?,?)";
       PreparedStatement cmd = con.prepareStatement(sql);
       cmd.setString(1,"张三");
       cmd.setInt(2,25);
       cmd.executeUpdate();
       con.close();
    }
}
