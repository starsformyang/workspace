package cn.pzhuweb.jdbc;

import com.mysql.cj.protocol.Resultset;

import java.sql.*;

public class Demo1 {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://127.0.0.1:3306/test";
        Connection con = DriverManager.getConnection(url,"root","root");

        Statement cmd = con.createStatement();
        String sql = "select * from fortest";
        ResultSet rs = cmd.executeQuery(sql);

        while(rs.next()) {
            String name = rs.getString(1);
            int age = rs.getInt(2);
            System.out.println(name + "  " + age );
        }
    }
}
